#include "pch.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <math.h>
#include <string>
#include <sstream>

using namespace std;

class Log {
public:
	static void info(string message = "") {
		cout << message << endl;
	}
};

class Pawn {
public:
	int position;
	string name;

	Pawn() {}

	Pawn(string name) {
		this->name = name;
		this->position = 0;

		Log::info(name + " joined the game.");
	}
};

class Dice {
	int walls = 0;
	bool next = false;
public:
	int wall()
	{
		if (walls == 0)
		{
			do
			{
				cout << "Podaj ilosc scian:";
				cin >> walls;
				if (walls < 4 || walls>20)
				{
					cout << "Ilosc scian musi byc wieksza od 4 i mniejsza od 20\n";
				}
				else
				{
					next = true;
				}
			} while (next != true);
		}
		return walls;
	}
	int roll() {
		
		int result = (rand() % walls) + 1;

		ostringstream ss;
		ss << "Dice roll: " << result;
		Log::info(ss.str());

		return result;
	}
};



class Board {

public:

	vector<Pawn> pawns;
	Dice dice;
	Pawn winner;
	int turnsCounter;

	Board() {
		this->turnsCounter = 0;
	}

	void performTurn(int turn) {
		int maxPosition = turn;
		this->turnsCounter++;

		ostringstream ss;
		ss << "Turn " << this->turnsCounter;
		Log::info();
		Log::info(ss.str());

		for (int i = 0; i < this->pawns.size(); i++) {
			int walls = this->dice.wall();
			int rollResult = this->dice.roll();

			Pawn &pawn = this->pawns.at(i);

			if (rollResult == 1 && pawn.position % 2 != 0)
			{
				int rollResult = this->dice.roll();
				cout << pawn.name << " cofniety o: " << rollResult << "\n";
				pawn.position -= rollResult;
				ostringstream ss;
				ss << pawn.name << " new position: " << pawn.position;
				Log::info(ss.str());
			}
			else if (rollResult == walls && pawn.position % 10 == 0)
			{
				rollResult += (rand() % walls) + 1;
				cout << "Dice + Extra roll: " << rollResult << "\n";
				pawn.position += rollResult;
				ostringstream ss;
				ss << pawn.name << " new position: " << pawn.position;
				Log::info(ss.str());
			}
			else
			{
				pawn.position += rollResult;
				ostringstream ss;
				ss << pawn.name << " new position: " << pawn.position;
				Log::info(ss.str());
			}




			if (pawn.position >= maxPosition) {
				this->winner = pawn;
				throw "Winner was called!";
			}
		}

	}
};

int main() {
	int players = 0, turn = 0, walls = 0;
	bool next = false;
	string player;

	do
	{
		cout << "Podaj ilosc graczy:\n";
		cin >> players;
		if (players < 3 || players>10)
		{
			cout << "Ilosc graczy musi byc wieksza od 3 i mniejsza od 10\n";
		}
		else
		{
			next = true;
		}
	} while (next != true);

	next = false;



	next = false;

	do
	{
		cout << "Podaj liczbe pol:\n";
		cin >> turn;
		if (turn < 50 || turn>1000)
		{
			cout << "Ilosc tur musi byc wieksza od 50 i mniejsza od 1000\n";
		}
		else
		{
			next = true;
		}
	} while (next != true);



	Board board = Board();

	board.dice = Dice();
	for (int i = 0; i < players; i++)
	{
		cout << "Podaj nick gracza:\n";
		cin >> player;
		board.pawns.push_back(Pawn(player));
	}

	srand(time(NULL));



	try {
		while (true) {
			board.performTurn(turn);
		}
	}
	catch (const char* exception) {
		Log::info();
		Log::info(board.winner.name + " won");
	}

	return 0;
}
