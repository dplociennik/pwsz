package Notebook;

import Notebook.Entries.DrankBeer;
import Notebook.Entries.WatchedMovie;
import Notebook.Entries.Girls;
import Notebook.Entries.Cars;
import Notebook.Entries.Shopping;
import Notebook.Interfaces.NotebookEntry;

import java.util.Scanner;

final public class Menu {

    private Notebook notebook;
    private String test;

    public Menu(Notebook notebook) {

        this.notebook = notebook;
    }

    public void run() {
        boolean running = true;
        boolean running2 = true;
        Scanner scanner = new Scanner(System.in);

        while(running) {
            displayMenu();

            write("Wybierz opcję: ");
            switch(scanner.nextLine()) {
                case "1": {
                    write();
                    comment("zanotowano:");
                    for(NotebookEntry item : notebook.getAll()) {
                              write(item.getSlug());
                    }
                    break;
                }
                case "2": {
                    running2 = true;
                            while(running2) {
                                displaySecondMenu();

                                write("Wybierz opcję: ");
                                switch(scanner.nextLine()) {
                                    case "1": {
                                        
                                        write("Podaj tytuł filmu: ");
                                        String title = scanner.nextLine();
                                        WatchedMovie movie = new WatchedMovie(title);
                                        notebook.save(movie);
                                        break;

                                    }
                                    case "2": {
                                        
                                        write("Podaj nazwę piwa: ");
                                        String name = scanner.nextLine();
                                        write("Podaj nazwę browaru: ");
                                        String brewery = scanner.nextLine();
                                        DrankBeer beer = new DrankBeer(name, brewery);
                                        notebook.save(beer);
                                        break;

                                    }
                                    case "3": {
                                        
                                        
                                        write("Podaj imię dziewczyny: ");
                                        String first_name = scanner.nextLine();
                                        Girls girl = new Girls(first_name);
                                        notebook.save(girl);
                                        break;


                                    }
                                    case "4": {
                                        
                                        write("Podaj model Twojego auta: ");
                                        String model = scanner.nextLine();
                                        Cars car = new Cars(model);
                                        notebook.save(car);
                                        break;


                                    }
                                    case "5": {
                                        
                                        write("Podaj nazwę sklepu: ");
                                        String shop = scanner.nextLine();
                                        write("Podaj liste zakupów (po przecinku): ");
                                        String things = scanner.nextLine();
                                        Shopping shop_list = new Shopping(shop, things);
                                        notebook.save(shop_list);
                                        break;


                                    }
                                    case "b": {
                                        run();
                                    }
                                    default: {
                                        break;
                                    }
                                }
                            }
                }
                case "x": {
                    running = false;
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

    private void displayMenu() {
        write();
        comment("notatek: ");
        write("[1] wypisz notatki");
        write("[2] dodaj notatkę");
        write("[x] wyjdź");
    }
    
    private void displaySecondMenu() {
        write();
        comment("notatek: ");
        write("[1] zanotuj obejrzany film");
        write("[2] zanotuj wypite piwo");
        write("[3] zanotuj laskę");
        write("[4] zanotuj auta");
        write("[5] zanotuj zakupy");
        write("[b] wróć");
    }
    
    private void write() {
        System.out.println();
    }

    private void write(String $line) {
        System.out.println($line);
    }

    private void comment(String line) {
        write(line);
    }

}
