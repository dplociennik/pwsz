import random


class ForcePower(object):
    def __init__(self, name, damage):
        self.name = name
        self.damage = damage

    def __str__(self):
        return self.name + ": " + str(self.damage)

class ForceDefense(object):
    def __init__(self, name, defense):
        self.name = name
        self.defense = defense

    def __str__(self):
        return self.name + ": " + str(self.defense)

class ForceUser(object):
    life_points = 100
    available_movements = []

    def __init__(self, name):
        self.name = name
        self.available_movements = []
        self.available_defense = []

    def attack(self, opponent):
        drawnMovement = random.choice(self.available_movements)
        drawnDefense = random.choice(self.available_defense)
        noise = random.uniform(0.9, 1.1)
        damage = ((drawnMovement.damage * noise) * drawnDefense.defense)

        opponent.life_points -= damage

        print self.name + " attacks " + opponent.name + " with " + drawnMovement.name + " Defense: " + drawnDefense.name + "."
        print opponent.name + " now has " + str(opponent.life_points) + " life points."


class LightsaberUser(ForceUser):
    def __init__(self, name):
        super(LightsaberUser, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Lightsaber Attack", 10),
            ForcePower("Lightsaber Throw", 15)
        ])
        self.available_defense.extend([
            ForceDefense("Full defence", 0),
            ForceDefense("Defence 25%", 0.75),
            ForceDefense("No defence", 1),
        ])

class LightSideForceUser(ForceUser):
    def __init__(self, name):
        super(LightSideForceUser, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Mind Trick", 5),
        ])
        self.available_defense.extend([
            ForceDefense("Full defence", 0),
            ForceDefense("Defence 25%", 0.75),
            ForceDefense("No defence", 1),
        ])


class JediMaster(LightsaberUser, LightSideForceUser):
    def __init__(self, name):
        super(JediMaster, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Force Light", 35),
        ])
        self.available_defense.extend([
            ForceDefense("Full defence", 0),
            ForceDefense("Defence 25%", 0.75),
            ForceDefense("No defence", 1),
        ])


class DarkSideForceUser(ForceUser):
    def __init__(self, name):
        super(DarkSideForceUser, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Force Grip", 15),
            ForcePower("Force Lightning", 15)
        ])
        self.available_defense.extend([
            ForceDefense("Full defence", 0),
            ForceDefense("Defence 25%", 0.75),
            ForceDefense("No defence", 1),
        ])


class SithLord(LightsaberUser, DarkSideForceUser):
    def __init__(self, name):
        super(SithLord, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Force Rage", 30),
        ])
        self.available_defense.extend([
            ForceDefense("Full defence", 0),
            ForceDefense("Defence 25%", 0.75),
            ForceDefense("No defence", 1),
        ])