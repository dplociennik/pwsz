package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;

public class Courier implements EntityInterface {

    public String identify() {
        return "Courier";
    }

    public boolean canEnter() {
        return true;
    }

}
