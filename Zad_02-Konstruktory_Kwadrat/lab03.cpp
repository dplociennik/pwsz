#include "pch.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

class Point {
public:
	float x;
	float y;

	Point() {
		cout << "Point has been created." << endl;
	}

	Point(float x, float y) {
		this->x = x;
		this->y = y;
		cout << "Point [" << this->x << ", " << this->y << "] has been created." << endl;
	}

	~Point() {
		cout << "Point [" << this->x << ", " << this->y << "] has been deleted." << endl;
	}

	void movePoint(float xAxisShift, float yAxisShift) {
		this->x += xAxisShift;
		this->y += yAxisShift;
	}
};

class Circle {
public:
	Point center;
	float radius;

	Circle(Point center, float radius) {
		this->center = center;
		this->radius = radius;
	}

	void getCoordinates() {
		cout << "x: " << this->center.x << endl << "y: " << this->center.y << endl;
	}
};

class Square {
public:

	Square(float inputX, float inputY, int i, int inputRadius) {
		if (i == 2) { inputY += inputRadius; }
		if (i == 3) { inputX += inputRadius; }
		if (i == 4) { 
			inputX += inputRadius;
			inputY += inputRadius; 
		}

		cout << "Wierzcholek " << i << ": [" << "x: " << inputX << " y: " << inputY << "] " << endl;
	}

};

int main() {
	srand(time(NULL));

	float inputX = 0, inputY = 0;
	float inputRadius = 5;
	int n = 0;
	Point centerPoint = Point(inputX, inputY);;
	Circle circle = Circle(centerPoint, inputRadius);

	circle.center.movePoint(rand() % 10, rand() % 10);
	circle.getCoordinates();




	cout << "\nSquare" << endl;
	cout << "\nIle kwadratow chcesz zrobic?" << endl;
	cin >> n;


	for (int j = 1; j <= n; j++)
	{
		cout << "Kwadrat " << j << " a = " << inputRadius << endl;

		inputX = rand() % 10;
		inputY = rand() % 10;

		Square square = Square(inputX, inputY, 1, 0);

		for (int i = 2; i < 5; i++)
		{
			Square square = Square(inputX, inputY, i, inputRadius);
		}
		inputRadius = rand() % 10;
	}

	return 0;
}
