package ParkingLotManager;

import ParkingLotManager.Entities.Car;
import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Courier;
import ParkingLotManager.Entities.Pedestrian;
import ParkingLotManager.Entities.Privileged;
import ParkingLotManager.Entities.Tank;
import ParkingLotManager.Entities.TeacherCar;
import ParkingLotManager.Interfaces.EntityInterface;

import java.util.ArrayList;
import java.util.Random;

final public class ParkingLot {

    private ArrayList<EntityInterface> entitiesOnProperty = new ArrayList<>();
    private int carsOnProperty = 0;
    private int bicycleOnProperty = 0;
    private int courierOnProperty = 0;
    private int pedestrianOnProperty = 0;
    private int privilegedOnProperty = 0;
    private int tankOnProperty = 0;
    private int teacherCarOnProperty = 0;
    private int AllVehicles = 0;
    private int BusyPlaces = 0;
    private String[] blackList = new String[10];
    private static int ticketPrice = 30;
    private int profit = 0;
    private boolean carOnBlackList = false;
    private static int placesForBicycles = 3;
    private static int placesForCars = 20;
    private int minutes;
    private String timeEnter;

    
    public boolean checkIfCanEnter(EntityInterface entity) {
        return entity.canEnter();
    }
    
    public void letIn(EntityInterface entity) {
        entitiesOnProperty.add(entity);
        
        Random generator = new Random();
        

        minutes = generator.nextInt(59);
        if(minutes < 10) { timeEnter = (generator.nextInt(14)+8) + ":0" + minutes; }
        else { timeEnter = (generator.nextInt(14)+8) + ":" + minutes; }
        
        for(int i=0; i<blackList.length; i++)
        {
            blackList[i] = "Car with plate number DLX " + (generator.nextInt(89999) + 10000);
            if(entity.identify() == blackList[i])
            {
                carOnBlackList = true;
            }
            else
            {
                carOnBlackList = false;
            }
        }
        
        if(carOnBlackList == true)
        {
           Log.info(entity.identify() + " is on black list. " + timeEnter);
        }
        else if(entity.identify() == "Unknown bicycle" && placesForBicycles <= bicycleOnProperty)
        {
            Log.info(entity.identify() + " no enought places. " + timeEnter);
        }
        else
        {
            if(entity instanceof Car && placesForCars > carsOnProperty) {
                Log.info(entity.identify() + " let in. " + timeEnter);
                carsOnProperty++;

            }
            else if (entity instanceof Car && placesForCars <= carsOnProperty)
            {
                Log.info(entity.identify() + " no enought places. " + timeEnter);
            }
            else
            {
                Log.info(entity.identify() + " let in. " + timeEnter);
            }
        }

        if(entity instanceof Bicycle && placesForBicycles > bicycleOnProperty) {
            bicycleOnProperty++;
            
        }
        
        if(entity instanceof Courier) {
            courierOnProperty++;
            
        }
        
        if(entity instanceof Pedestrian) {
            pedestrianOnProperty++;
            
        }
        
        if(entity instanceof Privileged) {
            privilegedOnProperty++;
            
        }
        
        if(entity instanceof Tank) {
            tankOnProperty++;
            
        }
        
        if(entity instanceof TeacherCar) {
            teacherCarOnProperty++;
            
        }
        
    }
    
    
    public int countCars() {

        return carsOnProperty;
    }
    
    public int countBicycle() {

        return bicycleOnProperty;
    }
    
    public int countCourier() {

        return courierOnProperty;
    }
    
    public int countPedestrian() {

        return pedestrianOnProperty;
    }
    
    public int countPrivileged() {

        return privilegedOnProperty;
    }
    
    public int countTank() {

        return tankOnProperty;
    }
    
    public int countTeacherCar() {

        return teacherCarOnProperty;
    }
    
    public int countAllVehicles() {
        AllVehicles = teacherCarOnProperty+tankOnProperty+privilegedOnProperty+courierOnProperty+bicycleOnProperty+carsOnProperty;
        return AllVehicles;
    }
    
    public int countBusyPlaces() {
        AllVehicles = teacherCarOnProperty+tankOnProperty+bicycleOnProperty+carsOnProperty;
        return AllVehicles;
    }
    
    public int profit() {
        profit = ticketPrice * carsOnProperty;
        return profit;
    }
    
    public void countBlackList() {
        for (int i =0;i<blackList.length;i++){
            Log.info(blackList[i] + " is on black list");
        }

    }

}
