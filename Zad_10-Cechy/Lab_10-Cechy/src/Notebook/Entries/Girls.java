package Notebook.Entries;

import Notebook.Interfaces.NotebookEntry;
import Notebook.Traits.Slugger;

public class Girls implements NotebookEntry, Slugger {

    private String title;

    public Girls(String title) {
        this.title = title;
    }

    @Override
    public String getSlug() {
        return getSlug(title);
    }

}
