<?php

final class Library {

	protected $books;

	public function addNovel(Novel $novel): self {
		$this->books[] = $novel;
		return $this;
	}

	public function addComic(Comic $comic): self {
		$this->books[] = $comic;
		return $this;
	}

	public function addTextbook(Textbook $textbook): self {
		$this->books[] = $textbook;
		return $this;
	}

	public function getBooks(): array {
		return $this->books;
	}

}

abstract class Book {

	protected $title;

	public function __construct(string $title, $pages) {
		$this->title = $title;
		$this->pages = $pages;
	}

	public function getTitle(): string {
		return $this->title;
	}

	public function getPages(): int {
		return $this->pages;
	}

	abstract public function getType(): string;

}

class Novel extends Book {

	public function getType(): string {
		return "powieść";
	}

}

class Comic extends Book {

	public function getType(): string {
		return "komiks";
	}

}

class Textbook extends Book {

	public function getType(): string {
		return "podręcznik";
	}

}


$library = new Library();

if(isset($_POST['book_category']) && $_POST['book_category'] == 'novel')
{
	$library->addNovel(new Novel($_POST['book_title'], $_POST['book_pages']));
}
$library->addNovel(new Novel("Wektor pierwszy", "285"));
$library->addNovel(new Novel("Gwiazda po gwieździe", "312"));
$library->addNovel(new Novel("Jednocząca Moc", "310"));

if(isset($_POST['book_category']) && $_POST['book_category'] == 'comic')
{
	$library->addComic(new Comic($_POST['book_title'], $_POST['book_pages']));
}
$library->addComic(new Comic("Marvel", "87"));

if(isset($_POST['book_category']) && $_POST['book_category'] == 'textbook')
{
	$library->addTextbook(new Textbook($_POST['book_title'], $_POST['book_pages']));
}
$library->addTextbook(new Textbook("Symphony", "128"));

echo '<div class="container mt-4">
		<table class="table table-dark">',
  			'<thead>',
    			'<tr>',
    				'<th scope="col">Kategoria</th>',
    				'<th scope="col">Tytuł</th>',
    				'<th scope="col">Liczba stron</th>',
    			'</tr>',
    		'</thead>',
    		'<tbody>';
foreach($library->getBooks() as $book) {

	echo '<tr style="'; if(isset($_POST['show_book']) && $_POST['show_book'] != $book->getType()){echo 'display:none;';} echo '">';
	echo '<th scope="row">' . $book->getType() . '</th>';
	echo '<td>' . $book->getTitle() . '</td>';
	echo '<td>' . $book->getPages() . '</td>';
	echo '</tr>';		

} 
echo '</tbody>';
echo '</table>';
echo '</div>';
?>




<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Twoja domowa bilbioteka</title>
  </head>
  <body class="bg-dark text-white">


  	<div class="row mt-5">
  		
  		<div class="col-3 offset-2">
  			
  			<form action="" method="post">

			  <div class="form-group">
			    <label for="show_book">Pokaż książki z kategorii:</label>
			    <select class="form-control" name="show_book" id="show_book">
			      <option value="powieść">Powieść</option>
			      <option value="komiks">Komiks</option>
			      <option value="podręcznik">Podręcznik</option>
			    </select>
			  </div>

			  <button type="submit" class="btn btn-primary">Wyświetl książki</button>

			</form>

  		</div>

  		<div class="col-3 offset-2">
  			<form action="" method="post">

			  <div class="form-group">
			    <label for="book_category">Kategoria ksiązki:</label>
			    <select class="form-control" name="book_category" id="book_category">
			      <option value="novel">Powieść</option>
			      <option value="comic">Komiks</option>
			      <option value="textbook">Podręcznik</option>
			    </select>
			  </div>

			  <div class="form-group">
			    <label for="book_title">Tytuł ksiązki:</label>
			    <input type="text" name="book_title" class="form-control" id="book_title" placeholder="Wprowadź tytuł książki do Twojej biblioteki">
			  </div>

			  <div class="form-group">
			    <label for="book_pages">Podaj liczbę stron:</label>
			    <input type="text" name="book_pages" class="form-control" id="book_pages" placeholder="Wprowadź liczbę stron">
			  </div>

			  <button type="submit" class="btn btn-primary">Dodaj książke</button>
			</form>
		</div>
  	</div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>