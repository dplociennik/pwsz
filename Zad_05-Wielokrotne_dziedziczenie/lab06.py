import random
from force_users import *

champions_number = input("Podaj ilosc graczy (max 10): ")

names_champions = [
]

champions = [
]

died_champions = [
]

i = 0
while i < champions_number:
    names_champions.append("Player%d" % (i, ))
    i += 1

for name in names_champions:
    random_number = random.randint(0, 4)
    if random_number == "0":
        champions.append(LightsaberUser(name))
    elif random_number == "1":
        champions.append(LightSideForceUser(name))
    elif random_number == "2":
        champions.append(DarkSideForceUser(name))
    elif random_number == "3":
        champions.append(JediMaster(name))
    else:
        champions.append(SithLord(name))




while len(champions) > 1:
    attacker = random.choice(champions)
    target = attacker

    while attacker == target:
        target = random.choice(champions)

    attacker.attack(target)
    if target.life_points <= 0:
        print target.name + " died."
        champions.remove(target)
        died_champions.append(target.name)

print ""
print "The winner is " + champions[0].name

for died in died_champions:
    print died
