package Notebook.Entries;

import Notebook.Interfaces.NotebookEntry;
import Notebook.Traits.Slugger;

public class Shopping implements NotebookEntry, Slugger {

    private String shop;
    private String shop_list;

    public Shopping(String shop, String shop_list) {
        this.shop = shop;
        this.shop_list = shop_list;
    }

    @Override
    public String getSlug() {
        return getSlug(shop + " " + shop_list);
    }

}
