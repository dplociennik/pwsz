package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;

public class Privileged implements EntityInterface {

    public String identify() {
        return "Privileged";
    }

    public boolean canEnter() {
        return true;
    }

}
