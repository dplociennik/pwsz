
#include "pch.h"
#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h> 
#include <time.h>

using namespace std;

#define STUDENTS_COUNT 5

class Student {
public:
	string studentNo;
	string studentName;
	void setStudentNo(string studentNo) {
		this->studentNo = studentNo;
	}

	string getStudentNo() {
		return this->studentNo;
	}

	void setStudentName(string studentName) {
		this->studentName = studentName;
	}

	string getStudentName() {
		return this->studentName;
	}

};

string getRandomStudentNumber() {
	ostringstream ss;
	int randomNumber = rand() % 2000 + 37000;

	ss << randomNumber;

	return ss.str();
}

string getRandomStudentName() {

	int randomNumber1 = rand() % 3;

	string StudentsName[3];
	StudentsName[0] = "Dawid Plociennik";
	StudentsName[1] = "Karol Zygadlo";
	StudentsName[2] = "Kamil Piech";


	return StudentsName[randomNumber1];
}

int main() {
	vector<Student> students;

	for (int i = 0; i < STUDENTS_COUNT; i++) {
		Student student;
		student.setStudentNo(getRandomStudentNumber());
		student.setStudentName(getRandomStudentName());
		students.push_back(student);
	}

	cout << "Students group have been filled." << endl << endl;

	for (int i = 0; i < students.size(); i++) {
		Student student = students.at(i);

		cout << student.getStudentName() << " Numer indeksu: " << student.getStudentNo() << endl;
	}

	return 0;
}

