package ParkingLotManager;

import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Car;
import ParkingLotManager.Entities.Pedestrian;
import ParkingLotManager.Entities.TeacherCar;
import ParkingLotManager.Entities.Courier;
import ParkingLotManager.Entities.Privileged;
import ParkingLotManager.Entities.Tank;
import ParkingLotManager.Interfaces.EntityInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class QueueGenerator {
    

    public static ArrayList<EntityInterface> generate() {
        ArrayList<EntityInterface> queue = new ArrayList<>();
        
        Random random = new Random();
        int ANONYMOUS_PEDESTRIANS_COUNT = random.nextInt(50);
        for (int i = 0; i <= ANONYMOUS_PEDESTRIANS_COUNT; i++) {
            queue.add(new Pedestrian());
        }
        
        int PEDESTRIANS_COUNT = random.nextInt(100);
        for (int i = 0; i <= PEDESTRIANS_COUNT; i++) {
            queue.add(new Pedestrian(getRandomName()));
        }
        
        int CARS_COUNT = random.nextInt(50);
        for (int i = 0; i <= CARS_COUNT; i++) {
            queue.add(new Car(getRandomPlateNumber()));
        }
        
        int TEACHER_CARS_COUNT = random.nextInt(20);
        for (int i = 0; i <= TEACHER_CARS_COUNT; i++) {
            queue.add(new TeacherCar(getRandomPlateNumber()));
        }
        
        int BICYCLES_COUNT = random.nextInt(15);
        for (int i = 0; i <= BICYCLES_COUNT; i++) {
            queue.add(new Bicycle());
        }
        
        int COURIER_COUNT = random.nextInt(5);
        for (int i = 0; i <= COURIER_COUNT; i++) {
            queue.add(new Courier());
        }
        
        int PRIVILEGED_COUNT = random.nextInt(3);
        for (int i = 0; i <= PRIVILEGED_COUNT; i++) {
            queue.add(new Privileged());
        }
        
        int TANK_COUNT = random.nextInt(3);
        for (int i = 0; i <= TANK_COUNT; i++) {
            queue.add(new Tank());
        }
        
        Collections.shuffle(queue);

        return queue;
    }

    private static String getRandomPlateNumber() {
        Random generator = new Random();
        return "DLX " + (generator.nextInt(89999) + 10000);
    }

    private static String getRandomName() {
        String[] names = {"John", "Jack", "James", "George", "Joe", "Jim"};
        return names[(int) (Math.random() * names.length)];
    }

}
