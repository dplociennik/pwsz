import ParkingLotManager.Interfaces.EntityInterface;
import ParkingLotManager.Log;
import ParkingLotManager.ParkingLot;
import ParkingLotManager.QueueGenerator;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        
        for (int i = 0; i < 3; i++)
        {
                  
        ArrayList<EntityInterface> queue = QueueGenerator.generate();
        ParkingLot parking = new ParkingLot();

        Log.info("There's " + parking.countCars() + " cars in the parking lot");
        Log.info();

        for (EntityInterface entity : queue) {
            if(parking.checkIfCanEnter(entity)) {
                parking.letIn(entity);
            }
        }

        Log.info();
        Log.info("There's " + parking.countCars() + " cars in the parking lot");
        Log.info("There's " + parking.countBicycle() + " bicycle in the parking lot");
        Log.info("There's " + parking.countCourier() + " courier in the parking lot");
        Log.info("There's " + parking.countPedestrian() + " pedestrian in the parking lot");
        Log.info("There's " + parking.countPrivileged() + " privileged in the parking lot");
        Log.info("There's " + parking.countTank() + " tank in the parking lot");
        Log.info("There's " + parking.countTeacherCar() + " teacher car in the parking lot");
        
        Log.info();
        Log.info("There's " + parking.countAllVehicles() + " vehicles in the parking lot");
        Log.info("There's " + parking.countBusyPlaces() + " busy places in the parking lot");
        
        Log.info();
        Log.info("Profit " + parking.profit() + " PLN");
        Log.info("Generator stared " + (i+1) + " times");
        
        Log.info();
        Log.info("Black list:"); parking.countBlackList();
        

        }

    }
}
