package Notebook.Entries;

import Notebook.Interfaces.NotebookEntry;
import Notebook.Traits.Slugger;

public class Cars implements NotebookEntry, Slugger {

    private String title;

    public Cars(String title) {
        this.title = title;
    }

    @Override
    public String getSlug() {
        return getSlug(title);
    }

}
